export default class BaseRequest {
  constructor () {
    this.api = 'https://noradio.ru/api'
    this.api = 'http://127.0.0.1:8000/api'
  }

  toQueryParams (obj) {
    return Object.keys(obj).map(k => `${k}=${obj[k]}`).join('&')
  }

  async makeRequest (url, method = 'GET', data = null) {
    const fetchOpts = {
      headers: {
        'Content-Type': 'application/json'
      },
      body: data ? JSON.stringify(data) : null,
      method
    }

    const res = await fetch(url, fetchOpts).then(r => r.json()).catch(e => console.error(e))

    return res
  }
}
