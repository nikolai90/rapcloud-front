import BaseRequest from '../BaseRequest'

class Auth extends BaseRequest {
  signIn (credentials) {
    return this.makeRequest(`${this.api}/auth/login`, 'POST', credentials)
  }

  createUser (id) {
    return this.makeRequest(`${this.api}/auth/reg`, 'POST')
  }

  apiAuth (query) {
    return this.makeRequest(`${this.api}/auth/me`, 'POST')
  }
}

export default new Auth()
