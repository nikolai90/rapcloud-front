import BaseRequest from '../BaseRequest'

class Authors extends BaseRequest {
  get ({ page }) {
    return this.makeRequest(`${this.api}/authors?${this.toQueryParams({ page })}`, 'GET')
  }

  getById (id) {
    return this.makeRequest(`${this.api}/authors/${id}`, 'GET')
  }

  search (query) {
    return this.makeRequest(`${this.api}/authors/search?query=${query}`, 'GET')
  }
}

export default new Authors()
