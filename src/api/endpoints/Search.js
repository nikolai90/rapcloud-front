import BaseRequest from '../BaseRequest'

class Search extends BaseRequest {
  query (q) {
    return this.makeRequest(`${this.api}/tracks/search?${this.toQueryParams({ query: q })}`, 'GET')
  }
}

export default new Search()
