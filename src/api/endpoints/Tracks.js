import BaseRequest from '../BaseRequest'

class Tracks extends BaseRequest {
  getByAuthor (id) {
    return this.makeRequest(`${this.api}/tracks?author=${id}}`, 'GET')
  }
}

export default new Tracks()
