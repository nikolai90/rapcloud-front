import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import uiComponentsArr from './components/ui'
import infScroll from '@/utils/directives/inf-scroll'
const app = createApp(App).use(store).use(router)
app.directive('infinity-scroll', infScroll)
uiComponentsArr.forEach(c => app.component(c.name, c.component))

const token = localStorage.getItem('authToken')
store.dispatch('auth/A_ME_DATA', token).then(() => {
  app.mount('#app')
})
