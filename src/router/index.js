import store from '@/store'
import { createRouter, createWebHistory } from 'vue-router'
import Search from '../views/Search.vue'

const routes = [
  {
    path: '/',
    name: 'Search',
    component: Search
  },
  {
    path: '/authors',
    name: 'Authors',
    component: () => import(/* webpackChunkName: "authors" */ '../views/Authors.vue')
  },
  {
    path: '/authors/:id',
    name: 'Author-detail',
    component: () => import(/* webpackChunkName: "author-dedail" */ '../views/AuthorDetail.vue')
  },
  {
    path: '/signin',
    name: 'Signin',
    component: () => import(/* webpackChunkName: "author-dedail" */ '../views/Signin.vue')
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () => import(/* webpackChunkName: "author-dedail" */ '../views/Signup.vue')
  },
  {
    path: '/likes',
    name: 'Likes',
    component: () => import(/* webpackChunkName: "likes" */ '../views/Likes.vue'),
    meta: {
      needAuth: true
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.needAuth) {
    store.commit('SET_PAGE_AFTER_LOGGED_IN', to.path)
    next('/signin')
  } else {
    next()
  }
})

export default router
