export default {
  namespaced: true,
  state: {
    asideNavShowed: false
  },
  mutations: {
    SET_ASIDE_NAV_SHOWED (state, payload) {
      state.asideNavShowed = payload
    }
  },
  actions: {
    A_TOGGLE_NAVIGATION_SHOWED ({ commit, state }) {
      commit('SET_ASIDE_NAV_SHOWED', !state.asideNavShowed)
    }
  }
}
