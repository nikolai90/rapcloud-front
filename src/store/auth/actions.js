import * as types from './types'
// import * as mutation_types from './mutation_types'
import apiAuth from '@/api/endpoints/Auth'

export default {
  async [types.A_SIGN_IN] ({ commit }, credentials) {
    try {
      const res = await apiAuth.signIn(credentials)
      commit('SET_TOKEN', res.data.token)
      commit('SET_USER', res.data.user)
      return true
    } catch (e) {
      return {
        errorMessage: e.response.data.message
      }
    }
  },
  async [types.A_REGISTER_USER] ({ commit }, credentials) {
    try {
      const res = await apiAuth.createUser(credentials)
      commit('SET_TOKEN', res.data.token)
      commit('SET_USER', res.data.user)
      return true
    } catch (e) {
      return {
        errorMessage: e.response.data
      }
    }
  },
  async [types.A_ME_DATA] ({ commit }, token) {
    if (!token) return
    try {
      const res = await apiAuth.me()
      commit('SET_USER', res.data)
      return true
    } catch (e) {
      commit('SET_TOKEN', null)
      commit('SET_USER', null)
      return false
    }
  },
  async [types.A_UPDATE_USER] ({ commit }, data) {
    try {
      const res = await apiAuth.updateUser(data)
      commit('SET_USER', res.data)
      return true
    } catch (e) {
      return {
        errorObject: e.response.data
      }
    }
  },
  [types.A_LOG_OUT] ({ commit }) {
    // TODO отправлять запрос на роут logout
    commit('SET_USER', null)
    commit('SET_TOKEN', null)
  }
}
