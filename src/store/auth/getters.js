const GETTERS = {
  userAuthorized (state) {
    return !!(state.user && state.token)
  }
}

export default GETTERS
