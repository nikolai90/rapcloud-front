// eslint-disable-next-line camelcase
import * as mutation_types from './mutation_types'
export default {
  [mutation_types.SET_TOKEN] (state, payload) {
    state.token = payload
  },
  [mutation_types.SET_USER] (state, payload) {
    state.user = payload
  },
  SET_PAGE_AFTER_LOGGED_IN (state, payload) {
    state.pageAfterLoggedIn = payload
  }
}
