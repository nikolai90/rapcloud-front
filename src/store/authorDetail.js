import apiAuthors from '@/api/endpoints/Authors'
import apiTracks from '@/api/endpoints/Tracks'
export default {
  namespaced: true,
  state: {
    author: null,
    authorSongs: []
  },
  mutations: {
    SET_AUTHOR (state, payload) {
      state.author = payload
    },
    SET_AUTHOR_SONGS (state, payload) {
      state.authorSongs = payload
    }
  },
  actions: {
    async fetchAuthor ({ commit }, id) {
      const res = await apiAuthors.getById(id)
      commit('SET_AUTHOR', res)
    },
    async fetchAuthorSongs ({ commit }, id) {
      const res = await apiTracks.getByAuthor(id)
      commit('SET_AUTHOR_SONGS', res)
    }
  }
}
