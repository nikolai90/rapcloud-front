import apiAuthors from '@/api/endpoints/Authors'
export default {
  namespaced: true,
  state: {
    authors: [],
    currentPage: 1
  },
  mutations: {
    SET_AUTHORS (state, payload) {
      state.authors = payload
    },
    SET_CURRENT_PAGE (state, payload) {
      state.currentPage = payload
    }
  },
  actions: {
    async fetchAuthors ({ commit, state }) {
      const res = await apiAuthors.get({ page: state.currentPage })
      commit('SET_AUTHORS', state.authors.concat(res.data))
    },
    async searchAuthors ({ commit }, q) {
      const res = await apiAuthors.search(q)
      commit('SET_AUTHORS', res.data)
      commit('currentPage', 1)
    }
  }
}
