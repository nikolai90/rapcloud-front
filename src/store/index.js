import { createStore } from 'vuex'
import search from './search'
import player from './player'
import authors from './authors'
import author from './authorDetail'
import auth from './auth/store'
import asideNav from './aside-nav'
export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    asideNav,
    search,
    player,
    authors,
    author,
    auth
  }
})
