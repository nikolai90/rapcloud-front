import Player from '../utils/Howl'
export default {
  namespaced: true,
  state: {
    playlist: [],
    player: null,
    idAudioCurrent: null,
    seekCurrent: 0,
    durationCurrent: 0,
    isPlaying: false,
    volume: 0.5
  },
  mutations: {
    SET_PLAY_LIST (state, payload) {
      state.playlist = payload
    },
    SET_PLAYER (state, payload) {
      state.player = payload
    },
    SET_ID_AUDIO_CURRENT (state, payload) {
      state.idAudioCurrent = payload
    },
    SET_SEEK_CURRENT (state, payload) {
      state.seekCurrent = payload
    },
    SET_DURATION_CURRENT (state, payload) {
      state.durationCurrent = payload
    },
    SET_IS_PLAYING (state, payload) {
      state.isPlaying = payload
    },
    SET_CURRENT_VOLUME (state, payload) {
      state.volume = payload
    }
  },
  getters: {
    audioCurrentData: (state) => state.playlist.find(a => a.id === state.idAudioCurrent)
  },
  actions: {
    initPlayer ({ commit, state }) {
      if (state.player) state.player.pause()
      const playlist = state.playlist.map(i => ({ url: i.data }))
      const player = new Player(playlist)
      commit('SET_PLAYER', player)
    },
    updateVolume ({ state }, payload) {
      if (!state.player) return
      let v = payload
      if (v < 0) v = 0
      state.player.updateVolume(v)
    },
    setSeek ({ state }, v) {
      if (state.player) {
        state.player.setSeek(v)
      }
    },
    destroyPlayer ({ commit, state }) {
      state.player.playPause(false)
    },
    playPause ({ state }, payload) {
      payload
        ? state.player.play()
        : state.player.pause()
    },
    async playAudio ({ commit, getters, state }, id) {
      commit('SET_ID_AUDIO_CURRENT', id)

      const index = state.playlist.findIndex(i => i.id === id)
      console.log('playAudio index:', index)
      state.player.skipTo(index)
    }
  }
}
