import apiSearch from '@/api/endpoints/Search'
export default {
  namespaced: true,
  state: {
    list: null
  },
  mutations: {
    SET_AUDIO_LIST (state, payload) {
      state.list = payload
    }
  },
  actions: {
    async execSearchQuery ({ commit }, q) {
      const res = await apiSearch.query(q)
      commit('SET_AUDIO_LIST', res.data)
      return res.data
    }
  }
}
