import { Howl } from 'howler'
import emitter from '@/utils/emitter'
import store from '@/store'
export default class Player {
  constructor (playlist) {
    this.playlist = playlist
    this.index = 0
  }

  play (index) {
    const i = index || index === 0 ? index : this.index
    const data = this.playlist[i]
    const self = this
    const options = {
      src: ['http://noradio.ru/storage/' + data.url],
      html5: true,
      onplay () {
        const sound = self.playlist[self.index].howl
        emitter.emit('SET_IS_PLAYING', true)
        emitter.emit('SET_DURATION_CURRENT', sound.duration())
        requestAnimationFrame(self.step.bind(self))
      },
      onend () {
        self.play(this.index + 2)
      }
    }

    const sound = data.howl = data.howl
      ? data.howl
      : new Howl(options)

    sound.volume(store.state.player.volume)
    sound.play()
    this.index = i
  }

  pause () {
    // const sound = this.playlist[this.index].howl
    this.playlist.forEach(player => {
      if (player.howl) player.howl.pause()
    })
    emitter.emit('SET_IS_PLAYING', false)
  }

  skipTo (index) {
    if (this.playlist[this.index].howl) {
      this.playlist[this.index].howl.stop()
    }

    this.play(index)
  }

  step () {
    const sound = this.playlist[this.index].howl
    if (sound.playing()) {
      setTimeout(() => {
        emitter.emit('SET_CURRENT_SEEK', sound.seek())
        this.step()
      }, 200)
    }
  }

  updateVolume (v) {
    const sound = this.playlist[this.index].howl
    sound.volume(v)
    emitter.emit('SET_CURRENT_VOLUME', v)
  }

  setSeek (v) {
    this.playlist.forEach(player => {
      if (player.howl) player.howl.seek(v)
    })
  }
}
