const infinityScroll = {
  beforeMount (el, binding) {
    const { instance } = binding

    window._vmInfScrollCallback = () => {
      const totalPageHeight = document.body.scrollHeight
      const scrollPoint = window.scrollY + window.innerHeight

      const preventCallbackPropName = el.getAttribute(
        'infinite-scroll-disabled'
      )
      const prevenCallback = instance[preventCallbackPropName]
      const callback = binding.value

      if (scrollPoint >= totalPageHeight) {
        if (!prevenCallback) {
          callback()
        }
      }
    }

    window.addEventListener('scroll', window._vmInfScrollCallback)
  },
  mounted () {},
  beforeUpdate () {}, // new
  updated () {},
  beforeUnmount () {}, // new
  unmounted () {
    window.removeEventListener('scroll', window._vmInfScrollCallback)
  }
}

export default infinityScroll
